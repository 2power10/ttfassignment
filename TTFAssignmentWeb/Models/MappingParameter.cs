﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTFAssignmentWeb
{
    public class MappingParameter
    {
        public Boolean A { get; set; }
        public Boolean B { get; set; }
        public Boolean C { get; set; }
        public Int32 D { get; set; }
        public Int32 E { get; set; }
        public Int32 F { get; set; }

    }
}
