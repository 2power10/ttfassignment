﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTFAssignmentWeb
{
    public enum Result
    {
        S,
        R,
        T
    }
    public class CalcRespnose
    {
        public Result X { get; set; }
        public double Y { get; set; }
    }
}
