﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTFAssignmentWeb.Logics
{
    public class Specialized2Mapper : BaseMapper
    {
        public override Result Step1(MappingParameter parameter)
        {
            if (parameter == null)
                throw new ArgumentException("parameter is null");

            Nullable<Result> result = null;
            if (parameter.A && parameter.B && !parameter.C)
                result = Result.T;
            else if (parameter.A && !parameter.B && parameter.C)
                result = Result.S;
            // if not fall into this specialized cases, use base class logic
            if (result == null)
                result = base.Step1(parameter);
            return result.Value;
        }

        public override double Step2(Result input, MappingParameter parameter)
        {
            if (parameter == null)
                throw new ArgumentException("parameter is null");

            double result = base.Step2(input, parameter);
            // override following cases
            if (input == Result.S)
                result = parameter.F + parameter.D +
                        (parameter.D * parameter.E / 100.0);
            return result;
        }
    }
}