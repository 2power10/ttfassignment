﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTFAssignmentWeb.Logics;

namespace TTFAssignmentWeb
{
    public class MapperFactory
    {
        static public IMapper GetMapper(MappingParameter param)
        {
            // Generate different mapper base on some rules necessary
            // Here just return the base mapper
            return new BaseMapper();
        }
    }
}
