﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTFAssignmentWeb.Logics
{
    public class Specialized1Mapper : BaseMapper
    {
        public override double Step2(Result input, MappingParameter parameter)
        {
            if (parameter == null)
                throw new ArgumentException("parameter is null");

            double result = base.Step2(input, parameter);
            // override following cases
            if (input == Result.R)
                result = 2 * parameter.D + (parameter.D * parameter.E / 100.0);
            return result;
        }
    }
}