﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTFAssignmentWeb.Logics
{
    public interface IMapper
    {
        CalcRespnose Mapping(MappingParameter parameter);
    }
    public class BaseMapper : IMapper
    {
        public CalcRespnose Mapping(MappingParameter parameter)
        {
            if (parameter == null)
                throw new ArgumentException("null mapping parameter");
            CalcRespnose response = new CalcRespnose();
            response.X = Step1(parameter);
            response.Y = Step2(response.X, parameter);
            return response;
        }
        public virtual Result Step1(MappingParameter parameter)
        {
            if (parameter.A && parameter.B && !parameter.C)
                return Result.S;
            else if (parameter.A && parameter.B && parameter.C)
                return Result.R;
            else if (!parameter.A && parameter.B && parameter.C)
                return Result.T;
            else
                throw new ArgumentException("argument not valid");
        }

        public virtual double Step2(Result input, MappingParameter parameter)
        {
            if (parameter == null)
                throw new ArgumentException("mapping parameter is null");

            switch (input)
            {
                case Result.S:
                    return parameter.D + (parameter.D * parameter.E / 100.0);
                case Result.R:
                    return parameter.D + (parameter.D * (parameter.E - parameter.F) / 100.0);
                case Result.T:
                    return parameter.D - (parameter.D * parameter.F / 100.0);
                default:
                    throw new NotImplementedException("new added enum type is not handled");
            }
        }
    }
}