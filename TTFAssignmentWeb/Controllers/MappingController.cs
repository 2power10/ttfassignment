﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TTFAssignmentWeb.Models;

namespace TTFAssignmentWeb.Controllers
{
    public class MappingController : ApiController
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(MappingController));
        [Route("AddItemToShrinkageReport")]
        [HttpGet]
        // assume all parameter is necessary in this calculation
        public IHttpActionResult Calc(Nullable<Boolean> A = null,
                                      Nullable<Boolean> B = null,
                                      Nullable<Boolean> C = null,
                                      Nullable<Int32> D = null,
                                      Nullable<Int32> E = null,
                                      Nullable<Int32> F = null)
        {
            if (!IsValid(A, B, C, D, E, F))
                return Content(HttpStatusCode.BadRequest, new Response() { errorMsg = "Invalid Parameter" });
            var parameter = new MappingParameter()
            {
                A = A.Value,
                B = B.Value,
                C = C.Value,
                D = D.Value,
                E = E.Value,
                F = F.Value
            };

            try
            {
                var mapper = MapperFactory.GetMapper(parameter);
                var resp = mapper.Mapping(parameter);
                return Json(resp);
            }
            catch (ArgumentException argException)
            {
                logger.Error(argException);
                return Content(HttpStatusCode.BadRequest, new Response() { errorMsg = "Invalid Parameter" });
            }
            catch (Exception exception)
            {
                logger.Error(exception);
                return Content(HttpStatusCode.InternalServerError, new Response() { errorMsg = "Internal Server Error" });
            }
        }

        private bool IsValid(Nullable<Boolean> A = null,
                             Nullable<Boolean> B = null,
                             Nullable<Boolean> C = null,
                             Nullable<Int32> D = null,
                             Nullable<Int32> E = null,
                             Nullable<Int32> F = null)
        {
            if (A == null ||
                B == null ||
                C == null ||
                D == null ||
                E == null ||
                F == null)
                return false;
            return true;
        }
    }
}
