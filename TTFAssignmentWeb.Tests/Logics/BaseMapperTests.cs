﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TTFAssignmentWeb.Logics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTFAssignmentWeb.Logics.Tests
{
    [TestClass()]
    public class BaseMapperTests
    {
        [TestMethod()]
        public void BaseMapperMappingTestError()
        {
            var baseMapper = new BaseMapper();
            try
            {
                baseMapper.Mapping(null);
            }
            catch (Exception exception)
            {
                Assert.AreEqual(exception.GetType(), typeof(ArgumentException));
                return;
            }
            Assert.Fail();
        }

        [TestMethod()]
        public void BaseMapperMappingTest()
        {
            var baseMapper = new BaseMapper();
            var testCases = new List<Tuple<MappingParameter, Result, double>>()
            {
                new Tuple<MappingParameter, Result, double>(new MappingParameter() { A = true, B = true, C = false, D = 123, E = 456, F = 789}, Result.S,  683.88),
                new Tuple<MappingParameter, Result, double>(new MappingParameter() { A = true, B = true, C = true, D = 123, E = 456, F = 789}, Result.R, -286.59),
                new Tuple<MappingParameter, Result, double>(new MappingParameter() { A = false, B = true, C = true, D = 123, E = 456, F = 789}, Result.T, -847.47),

            };
            foreach (var testCase in testCases)
            {
                var actualResult = baseMapper.Mapping(testCase.Item1);
                Assert.AreEqual(testCase.Item2, actualResult.X);
                Assert.AreEqual(testCase.Item3, actualResult.Y);
            }
        }

        [TestMethod()]
        public void BaseMapperStep1Test()
        {
            var baseMapper = new BaseMapper();
            var testCases = new List<Tuple<MappingParameter, Result>>()
            {
                new Tuple<MappingParameter, Result>(new MappingParameter() { A = true, B = true, C = false }, Result.S),
                new Tuple<MappingParameter, Result>(new MappingParameter() { A = true, B = true, C = true }, Result.R),
                new Tuple<MappingParameter, Result>(new MappingParameter() { A = false, B = true, C = true }, Result.T),

            };
            foreach (var testCase in testCases)
            {
                var actualResult = baseMapper.Step1(testCase.Item1);
                Assert.AreEqual(testCase.Item2, actualResult);
            }
        }

        [TestMethod()]
        public void BaseMapperStep1TestError()
        {
            var baseMapper = new BaseMapper();
            var mappingParamter = new MappingParameter()
            {
                A = false,
                B = true,
                C = false
            };
            try
            {
                baseMapper.Step1(mappingParamter);
            }
            catch(ArgumentException)
            {
                return;
            }
            Assert.Fail();
        }

        [TestMethod()]
        public void BaseMapperStep2Test()
        {
            var baseMapper = new BaseMapper();
            var testCases = new List<Tuple<Result, MappingParameter, double>>()
            {
                new Tuple<Result, MappingParameter, double>(Result.S, new MappingParameter() {D = 123, E = 456, F = 789 }, 683.88),
                new Tuple<Result, MappingParameter, double>(Result.R, new MappingParameter() {D = 123, E = 456, F = 789 }, -286.59),
                new Tuple<Result, MappingParameter, double>(Result.T, new MappingParameter() {D = 123, E = 456, F = 789 }, -847.47),
            };
            foreach (var testCase in testCases)
            {
                var actualResult = baseMapper.Step2(testCase.Item1, testCase.Item2);
                Assert.AreEqual(testCase.Item3, actualResult);
            }
        }
    }
}