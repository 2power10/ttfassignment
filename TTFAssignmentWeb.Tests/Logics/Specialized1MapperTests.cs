﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TTFAssignmentWeb.Logics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTFAssignmentWeb.Logics.Tests
{
    [TestClass()]
    public class Specialized1MapperTests
    {
        [TestMethod()]
        public void Specialized1MapperStep2Test()
        {
            var mapper = new Specialized1Mapper();
            var testCases = new List<Tuple<Result, MappingParameter, double>>()
            {
                new Tuple<Result, MappingParameter, double>(Result.S, new MappingParameter() {D = 123, E = 456, F = 789 }, 683.88),
                new Tuple<Result, MappingParameter, double>(Result.R, new MappingParameter() {D = 123, E = 456, F = 789 }, 806.88),
                new Tuple<Result, MappingParameter, double>(Result.T, new MappingParameter() {D = 123, E = 456, F = 789 }, -847.47),
            };
            foreach (var testCase in testCases)
            {
                var actualResult = mapper.Step2(testCase.Item1, testCase.Item2);
                Assert.AreEqual(testCase.Item3, actualResult);
            }
        }
    }
}