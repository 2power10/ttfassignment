﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TTFAssignmentWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTFAssignmentWeb.Tests
{
    [TestClass()]
    public class MapperFactoryTests
    {
        [TestMethod()]
        public void GetMapperTest()
        {
            var mapper = MapperFactory.GetMapper(new MappingParameter());
            Assert.IsNotNull(mapper);
        }
    }
}