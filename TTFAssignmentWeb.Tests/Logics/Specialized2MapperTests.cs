﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TTFAssignmentWeb.Logics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTFAssignmentWeb.Logics.Tests
{
    [TestClass()]
    public class Specialized2MapperTests
    {
        [TestMethod()]
        public void Specialized2MapperStep1Test()
        {
            var mapper = new Specialized2Mapper();
            var testCases = new List<Tuple<MappingParameter, Result>>()
            {
                new Tuple<MappingParameter, Result>(new MappingParameter() { A = true, B = true, C = false }, Result.T),
                new Tuple<MappingParameter, Result>(new MappingParameter() { A = true, B = true, C = true }, Result.R),
                new Tuple<MappingParameter, Result>(new MappingParameter() { A = false, B = true, C = true }, Result.T),
                new Tuple<MappingParameter, Result>(new MappingParameter() { A = true, B = false, C = true }, Result.S),

            };
            foreach (var testCase in testCases)
            {
                var actualResult = mapper.Step1(testCase.Item1);
                Assert.AreEqual(testCase.Item2, actualResult);
            }
        }

        [TestMethod()]
        public void Specialized2MapperStep2Test()
        {
            var mapper = new Specialized2Mapper();
            var testCases = new List<Tuple<Result, MappingParameter, double>>()
            {
                new Tuple<Result, MappingParameter, double>(Result.S, new MappingParameter() {D = 123, E = 456, F = 789 }, 1472.88),
                new Tuple<Result, MappingParameter, double>(Result.R, new MappingParameter() {D = 123, E = 456, F = 789 }, -286.59),
                new Tuple<Result, MappingParameter, double>(Result.T, new MappingParameter() {D = 123, E = 456, F = 789 }, -847.47),
            };
            foreach (var testCase in testCases)
            {
                var actualResult = mapper.Step2(testCase.Item1, testCase.Item2);
                Assert.AreEqual(testCase.Item3, actualResult);
            }
        }
    }
}